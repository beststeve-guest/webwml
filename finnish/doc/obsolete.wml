#use wml::debian::template title="Vanhentuneet oppaat"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/obsolete.defs"
#use wml::debian::translation-check translation="534d1b782cfb92f46dc41fd064f779fffc329b12"

<h1 id="historical">Historialliset oppaat</h1>

<p>Alla mainitut dokumentit on kirjoitettu kauan sitten eivätkä ne 
ole ajan tasalla tai ne on kirjoitettu Debianin edellisille versioille 
ja niitä ei ole päivitetty vastaamaan nykyisiä versioita. Niiden sisältämä 
tieto on vanhentunutta, mutta ne saattavat kuitenkin kiinnostaa joitakin.</p>


<h2 id="user">Käyttäjien oppaat</h2>

<document "dselect-perusteita aloittelijoille (englanninkielinen)" "dselect">

<div class="centerblock">
<p>
  Tämä dokumentti kertoo dselect-ohjelman käytöstä ensikertalaisille, ja sen 
  tarkoituksena on auttaa Debian-asennuksen onnistumisessa.  Se ei pyri 
  selittämään kaikkea, joten kun törmäät ensi kertaa dselect:iin, käytä 
  ohjelman ohjetta apunasi.
</p>
<doctable>
  <authors "Stéphane Bortzmeyer">
  <maintainer "(?)">
  <status>
  pysähdyksissä: <a href="https://packages.debian.org/aptitude">aptitude</a> on 
  korvannut dselectin Debianin pakettihallinnan vakiokäyttöliittymänä
  </status>
  <availability>
  <inddpvcs name="dselect-beginner" formats="html txt pdf ps"
            langs="ca cs da de en es fr hr it ja pl pt ru sk">
  </availability>
</doctable>
</div>

<hr />

<document "Käyttäjän opas (englanninkielinen)" "users-guide">

<div class="centerblock">
<p>
Tämä <q>käyttöopas</q> on uudelleen muotoiltu <q>Progeny-käyttöopas</q>. 
Sisältö on muutettu vastaamaan normaalia Debian-järjestelmää.</p>

<p>Yli 300 sivua sisältävä opas Debian-järjestelmän käytön aloittamisesta
graafisen- ja tekstikäyttöliittymän kautta.
</p>
<doctable>
  <authors "Progeny Linux Systems, Inc.">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  Sellaisenaan käyttökelpoinen opas. Kirjoitettu woodylle, vanhentunut.
  </status>
  <availability>
# langs="en" isn't redundant, it adds the needed language suffix to the link
	<inddpvcs name="users-guide" index="users-guide" langs="en" formats="html txt pdf">
  </availability>
</doctable>
</div>

<hr />

<document "Debian-pikaohje (englanninkielinen)" "tutorial">

<div class="centerblock">
<p>
Tämä opas on tarkoitettu uudelle Linux-käyttäjälle, asennuksen jälkeen 
tapahtuvaan Linuxiin tutustumiseen, tai uudelle Linux-käyttäjälle joka 
käyttää jonkun muun ylläpitämää järjestelmää.
</p>
<doctable>
  <authors "Havoc Pennington, Oliver Elphick, Ole Tetlie, James Treacy,
  Craig Sawyer, Ivan E. Moore II">
  <editors "Havoc Pennington">
  <maintainer "(?)">
  <status>
  pysähdyksissä; kesken;
  korvautunut mahdollisesti <a href="user-manuals#quick-reference">Debian-hakuteoksella</a>
  </status>
  <availability>
  ei vielä valmis
  <inddpvcs name="debian-tutorial" vcsname="tutorial" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian GNU/Linux: Asennus- ja käyttöohje (englanninkielinen)" "guide">

<div class="centerblock">
<p>
  Käyttöopas, suunnattu loppukäyttäjälle.
</p>
<doctable>
  <authors "John Goerzen, Ossama Othman">
  <editors "John Goerzen">
  <status>
  valmis (käsittelee potato-julkaisua)
  </status>
  <availability>
  <inoldpackage "debian-guide">
  </availability>
</doctable>
</div>

<hr />

<document "Debian-käyttäjän hakuteos (englanninkielinen)" "userref">

<div class="centerblock">
<p>
  Tämä opas tarjoaa ainakin yleiskuvan kaikesta mitä käyttäjän tulisi 
  tietää Debian GNU/Linux-järjestelmästään (esim. X:n asetukset, verkon 
  konfigurointi, levykeaseman käyttö jne.).  Sen tarkoituksena on kaventaa 
  Debian Tutorialin ja pakettien mukana tulevien yksityiskohtaisten man- 
  ja infosivujen välistä kuilua.</p>

  <p>Sen tarkoituksena on myös antaa jonkinlainen idea komentojen 
  yhdistelystä, yleisen Unix-periaatteen mukaisesti, <em>asiat voidaan 
  aina tehdä useammalla kuin yhdellä tavalla</em>.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Jason D. Waterman, Havoc Pennington,
      Oliver Elphick, Bruce Evry, Karl-Heinz Zimmer">
  <editors "Thalia L. Hooker, Oliver Elphick">
  <maintainer "(?)">
  <status>
  pysähdyksissä ja melko keskeneräinen;
  korvautunut mahdollisesti <a href="user-manuals#quick-reference">Debian-hakuteoksella</a>
  </status>
  <availability>
  <inddpvcs name="user">
  </availability>
</doctable>
</div>

<hr />

<document "Debian-järjestelmän ylläpitäjän opas (englanninkielinen)" "system">

<div class="centerblock">
<p>
  Tämä dokumentti on mainittu Policy-manualin esittelyssä.  
  Se kattaa kaikki Debian-järjestelmän ylläpitoon liittyvät näkökulmat.
</p>
<doctable>
  <authors "Tapio Lehtonen">
  <maintainer "(?)">
  <status>
  pysähdyksissä; kesken;
  korvautunut mahdollisesti <a href="user-manuals#quick-reference">Debian-hakuteoksella</a>
  </status>
  <availability>
  ei vielä saatavilla
  <inddpvcs name="system-administrator">
  </availability>
</doctable>
</div>

<hr />

<document "Debian-verkon ylläpitäjän opas (englanninkielinen)" "network">

<div class="centerblock">
<p>
  Tämä opas kattaa kaikki Debian-järjestelmän verkon ylläpitoon liittyvät 
  näkökulmat.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Duncan C. Thomson, Ivan E. Moore II">
  <maintainer "(?)">
  <status>
  pysähdyksissä; kesken;
  korvautunut mahdollisesti <a href="user-manuals#quick-reference">Debian-hakuteoksella</a>
  </status>
  <availability>
  ei vielä saatavilla
  <inddpvcs name="network-administrator">
  </availability>
</doctable>
</div>

<hr />

# Add this to books, there's a revised edition (2nd) @ Amazon
<document "Linux-keittokirja (The Linux Cookbook)" "linuxcookbook">

<div class="centerblock">
<p>
  Kädestä pitävä opas Debian GNU/Linux -järjestelmään opettaa 
  käytön jokapäiväisissä toimissa yli 1&nbsp;500 <q>reseptin</q> avulla. 
  Aiheina ovat tekstin, kuvien ja äänen käsittelystä tuottavuuteen 
  ja verkkoasioihin asti.  Kuten ohjelmisto, josta kirja kertoo, myös 
  itse kirja on tekijänoikeudella avattu ja sen lähdekoodi on saatavilla.
</p>
<doctable>
  <authors "Michael Stutz">
  <status>
  julkaistu; kirjoitettu woodylle, vanhentunut
  </status>
  <availability>
  <inoldpackage "linuxcookbook">
  <p><a href="http://dsl.org/cookbook/">tekijältä</a>
  </availability>
</doctable>
</div>

<document "APT HOWTO (englanninkielinen)" "apt-howto">

<div class="centerblock">
<p>
  Tämä opas pyrkii olemaan nopea mutta täydellinen tietolähde 
  APT-järjestelmästä ja sen ominaisuuksista.  Sisältää paljon tietoa 
  APT:n pääasiallisista käyttötarkoituksista ja lukuisia esimerkkejä.
</p>
<doctable>
  <authors "Gustavo Noronha Silva">
  <maintainer "Gustavo Noronha Silva">
  <status>
  valmis
  </status>
  <availability>
  <inoldpackage "apt-howto">
  <inddpvcs name="apt-howto" langs="en ca cs de es el fr it ja pl pt-br ru uk tr zh-tw zh-cn"
	   formats="html txt pdf ps" naming="locale" />
  </availability>
</doctable>
</div>

<document "Euro-tuki Debian GNU/Linuxissa (englanninkielinen)" "euro-support">

<div class="centerblock">
<p>
Tämä dokumentti käsittelee Debian GNU/Linux-käyttöjärjestelmän 
euro-tukeen liittyviä asioita ja opastaa kuinka järjestelmän ja sovellusten 
asetukset määritellään oikein tätä tarkoitusta varten.
</p>
<doctable>
  <authors "Javier Fernández-Sanguino Peña">
  <status>
  valmis
  </status>
  <availability>
  <inpackage "euro-support">
  <inddpvcs name="debian-euro-support" langs="en fr it" cvsname="euro-support">
  </availability>
</doctable>
</div>

<h2 id="devel">Kehittäjien oppaat</h2>

<document "Dpkg-opas (englanninkielinen)" "dpkgint">

<div class="centerblock">
<p>
  Dokumentti dpkg:n sisimmästä, ja siitä kuinka kirjoittaa uusia
  paketinhallintatyökaluja dpkg-kirjastoja käyttäen. Tämä lienee
  käyttökelpoinen vain rajatulle käyttäjäjoukolle.
</p>
<doctable>
  <authors "Klee Dienes">
  <status>
  pysähdyksissä
  </status>
  <availability>
  <inoldpackage "dpkg-doc">
  <a href="packaging-manuals/dpkg-internals/">HTML-versio</a>
  </availability>
</doctable>
</div>

<hr />

<document "Esittely: Debian-paketin tekeminen (englanninkielinen)" "makeadeb">

<div class="centerblock">
<p>
  Esittely siitä kuinka luodaan <code>.deb</code>-paketti, käyttämällä
  <strong>debmake</strong>-ohjelmaa.
</p>
<doctable>
  <authors "Jaldhar H. Vyas">
  <status>
  pysähdyksissä, vanhentunut, tilalle tullut <a href=
  "devel-manuals#maint-guide">New Maintainers' Guide</a>
  </status>
  <availability>
  <a href="https://people.debian.org/~jaldhar/">HTML-dokumentti</a>
  </availability>
</doctable>
</div>

<hr />

<document "Debian-ohjelmoijan opas (englanninkielinen)" "programmers">

<div class="centerblock">
<p>
  Avustaa uusia kehittäjiä luotaessa paketteja Debian GNU/Linux-järjestelmälle.
</p>
<doctable>
  <authors "Igor Grobman">
  <status>
  vanhentunut, tilalle tullut <a href="devel-manuals#maint-guide">Uuden ylläpitäjän opas</a>
  </status>
  <availability>
  <inddpvcs name="programmer">
  </availability>
</doctable>
</div>

<hr />

<document "Debianin pakettiopas (englanninkielinen)" "packman">

<div class="centerblock">
<p>
  Tämä opas kertoo Debianin binääri- ja lähdekoodipakettien luomiseen
  liittyvistä teknisistä näkökohdista. Se dokumentoi myös dselect:in ja
  sen hakutoimintoskriptien välistä liittymää. Se ei käsittele
  Debian-projektin linjausten vaatimuksia, ja olettaa dpkg:n
  toimintojen tuntemista järjestelmän ylläpitäjän näkökulmasta.

<doctable>
  <authors "Ian Jackson, Klee Dienes, David A. Morris, Christian Schwarz">
  <status>
  Osat jotka olivat de facto-käytäntöä yhdistettiin äskettäin
  <a href="devel-manuals#debian-policy">debian-policy-oppaaseen</a>.
  </status>
  <availability>
  <inoldpackage "packaging-manual">
  </availability>
</doctable>
</div>

<hr>

<document "Esittely kansainvälistämiseen (englanninkielinen)" "i18n">

<div class="centerblock">
<p>
  Tämä dokumentti käsittelee lokalisoinnin (l10n, localization),
  kansainvälistämisen (i18n, internationalization) ja monikielisyyden
  (m17n, multilingualization) perusideoita ja ohjeita ohjelmoijille
  ja pakettien ylläpitäjillle.

  <p>Tämän dokumentin tavoitteena on parantaa pakettien i18n-tukea
  ja tehdä Debianista yhä kansainvälisempi jakelu. Kaikista maailman kolkista
  tuleva apu on tervetullutta, sillä alkuperäinen kirjoittaja puhuu japania
  ja tämä dokumentti käsittelisi japanisointia (j10n, Japanization) jollei
  apua olisi saatu muita kieliä puhuvilta.

<doctable>
  <authors "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <maintainer "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <status>
  kehityksen alla
  </status>
  <availability>
  ei vielä valmis
  <inddpvcs-intro-i18n>
  </availability>
</doctable>
</div>

<hr>

<document "Kuinka ohjelmistokehittäjät voivat jakaa tuotteitaan suoraan .deb-formaatissa (englanninkielinen)" "swprod">

<div class="centerblock">
<p>
  Tämän dokumentin tarkoitus on selittää perusteet siitä, kuinka
  ohjelmistokehittäjät voivat integroida tuotteensa Debianiin, millaisia
  eri tilanteita tulee riippuen tuotteiden lisensseistä ja kehittäjien
  valinnoista ja mitkä ovat eri vaihtoehdot.  Tämä ei selitä kuinka
  paketteja luodaan, mutta linkittää dokumentteihin, joissa kerrotaan
  se kerrotaan.

  <p>Sinun kannattaa lukea tämä, jos laajempi kuva Debian-pakettien
  luomisesta ja jakelusta (ja mahdollisesti myös lisäämisestä
  Debian-jakeluun) ei ole tuttu.

<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  valmis (?)
  </status>
  <availability>
  <inddpvcs-distribute-deb>
  </availability>
</doctable>
</div>

<hr>

<document "Debian XML/SGML -linja (englanninkielinen)" "xml-sgml-policy">

<div class="centerblock">
<p>
  Alilinjaus Debian-paketeille, jotka tarjoavat ja/tai hyödyntävät XML-
  tai SGML-resursseja.

<doctable>
  <authors "Mark Johnson, Ardo van Rangelrooij, Adam Di Carlo">
  <status>
  käynnistysvaiheessa, mukaan liitetään nykyinen SGML-linja
  <tt>sgml-base-doc</tt>:ista ja uutta materiaalia XML-luetteloinnin hallintaan
  </status>
  <availability>
  <inddpvcs-xml-sgml-policy>
  </availability>
</doctable>
</div>

<hr>

<document "DebianDoc-SGML Markup-opas" "markup">

<div class="centerblock">
<p>
  Dokumentit <strong>debiandoc-sgml</strong>-järjestelmälle,
  sisältää parhaat käytännöt ja vinkit ylläpitäjille. Tulevat versiot
  sisältävät vihjeitä Debian-pakettien dokumenttien helpompaan ylläpitoon
  ja tekoon, ohjeita dokumenttien käännösten järjestelyyn ja muuta
  hyödyllistä tietoa.
  Katso myös <a href="https://bugs.debian.org/43718">vika #43718</a>.

<doctable>
  <authors "Ian Jackson, Ardo van Rangelrooij">
  <maintainer "Ardo van Rangelrooij">
  <status>
  valmis
  </status>
  <availability>
  <inpackage "debiandoc-sgml-doc">
  <inddpvcs-debiandoc-sgml-doc>
  </availability>
</doctable>
</div>


<h2 id="misc">Sekalaiset oppaat</h2>

<document "Debian META-opas (englanninkielinen)" "meta">

<div class="centerblock">
<p>
  Tämä opas kertoo käyttäjälle mistä löytää dokumentin, joka vastaa hänen
  kysymyksiinsä.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick">
  <maintainer "(?)">
  <status>
  suunnitteilla
  </status>
  <availability>
  ei vielä valmis, ylläpito ilmeisesti lopetettu
  <inddpvcs name="meta">
  </availability>
</doctable>
</div>

<hr />

<document "Debian-kirjaehdotuksia" "books">

<div class="centerblock">
<p>
  Luettelo kirjoista Debian-käyttäjien tai ylläpitäjien huomioitavaksi.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Tapio Lehtonen">
  <maintainer "?">
  <status>
  suunnitteilla
  </status>
  <availability>
  ei vielä valmis
  <inddpvcs name="book-suggestions">
  </availability>
</doctable>
</div>

<hr />

<document "Debian-sanasto" "dict">

<div class="centerblock">
<p>
  <q>Olen ollut pitkään sitä mieltä että tarvitsemme Debian-sanaston.
  Tällä projektilla on todellakin oma kummallinen kielensä. Ehkä voisimme
  laittaa jonnekin www-lomakkeen niin että ihmiset voisivat lähettää uusia
  termejä ja määrittelyjä niille aina kun niitä ilmenee.</q>
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick">
  <maintainer "?">
  <status>
  suunnitteilla
  </status>
  <availability>
  keskeneräinen
  <inddpvcs name="dictionary">
  <p>Paketit <a href="https://packages.debian.org/dict-vera">dict-vera</a>
  ja <a href="https://packages.debian.org/dict-foldoc">dict-foldoc</a>
  sisältävät runsaasti Debianiin liittyvien termien ja lyhenteiden määritelmiä.
  </availability>
</doctable>
</div>

<hr />

<dl>
  <dt><strong><a href="../releases/potato/installguide/">Potaton asennus-KUINKA</a></strong></dt>
    <dd>Epävirallinen asennusohje Debianin julkaisulle 2.2 (koodinimeltään potato).
    </dd>
</dl>

<hr>

<document "Debian-repository-HOWTO" "repo">

<div class="centerblock">
<p>
  Tämä dokumentti selittää kuinka Debian-pakettivarastot toimivat, kuinka 
  sellaisia luodaan ja kuinka ne lisätään <tt>sources.list</tt>:n oikein.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  valmis (?)
  </status>
  <availability>
  <inddpvcs name="repository-howto" index="repository-howto"
            formats="html" langs="en fr de uk ta">
  </availability>
</doctable>
</div>
