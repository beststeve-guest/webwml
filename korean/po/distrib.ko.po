# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Sangdo Jun, 2020
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml distrib\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-05-24 10:49+0900\n"
"Last-Translator: Sangdo Jun <sebuls@gmail.com>\n"
"Language-Team: debian-l10n-korean <debian-l10n-korean@lists.debian.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/distrib/search_contents-form.inc:9
#: ../../english/distrib/search_packages-form.inc:8
msgid "Keyword"
msgstr "키워드"

#: ../../english/distrib/search_contents-form.inc:13
msgid "Display"
msgstr "표시"

#: ../../english/distrib/search_contents-form.inc:16
msgid "paths ending with the keyword"
msgstr "키워드로 끝나는 경로"

#: ../../english/distrib/search_contents-form.inc:19
msgid "packages that contain files named like this"
msgstr "이런 파일 이름이 들어있는 패키지"

#: ../../english/distrib/search_contents-form.inc:22
msgid "packages that contain files whose names contain the keyword"
msgstr "이름에 키워드가 들어있는 파일이 들어있는 패키지"

#: ../../english/distrib/search_contents-form.inc:25
#: ../../english/distrib/search_packages-form.inc:25
msgid "Distribution"
msgstr "배포판"

#: ../../english/distrib/search_contents-form.inc:27
#: ../../english/distrib/search_packages-form.inc:27
msgid "experimental"
msgstr "실험적"

#: ../../english/distrib/search_contents-form.inc:28
#: ../../english/distrib/search_packages-form.inc:28
msgid "unstable"
msgstr "불안정"

#: ../../english/distrib/search_contents-form.inc:29
#: ../../english/distrib/search_packages-form.inc:29
msgid "testing"
msgstr "테스팅"

#: ../../english/distrib/search_contents-form.inc:30
#: ../../english/distrib/search_packages-form.inc:30
msgid "stable"
msgstr "안정"

#: ../../english/distrib/search_contents-form.inc:31
#: ../../english/distrib/search_packages-form.inc:31
msgid "oldstable"
msgstr "옛안정"

#: ../../english/distrib/search_contents-form.inc:33
msgid "Architecture"
msgstr "아키텍처"

#: ../../english/distrib/search_contents-form.inc:38
#: ../../english/distrib/search_packages-form.inc:32
#: ../../english/distrib/search_packages-form.inc:39
msgid "any"
msgstr ""

#: ../../english/distrib/search_contents-form.inc:48
#: ../../english/distrib/search_packages-form.inc:43
msgid "Search"
msgstr "검색"

#: ../../english/distrib/search_contents-form.inc:49
#: ../../english/distrib/search_packages-form.inc:44
msgid "Reset"
msgstr "리셋"

#: ../../english/distrib/search_packages-form.inc:12
msgid "Search on"
msgstr ""

#: ../../english/distrib/search_packages-form.inc:14
msgid "Package names only"
msgstr "패키지 이름 만"

#: ../../english/distrib/search_packages-form.inc:16
msgid "Descriptions"
msgstr "설명"

#: ../../english/distrib/search_packages-form.inc:18
msgid "Source package names"
msgstr "소스 패키지 이름"

#: ../../english/distrib/search_packages-form.inc:21
msgid "Only show exact matches"
msgstr "정확히 일치하는 것만 보이기"

#: ../../english/distrib/search_packages-form.inc:34
msgid "Section"
msgstr "섹션"

#: ../../english/distrib/search_packages-form.inc:36
msgid "main"
msgstr "main"

#: ../../english/distrib/search_packages-form.inc:37
msgid "contrib"
msgstr "contrib"

#: ../../english/distrib/search_packages-form.inc:38
msgid "non-free"
msgstr "non-free"

#: ../../english/releases/arches.data:8
msgid "Alpha"
msgstr "알파"

#: ../../english/releases/arches.data:9
msgid "64-bit PC (amd64)"
msgstr "64비트 PC (amd64)"

#: ../../english/releases/arches.data:10
msgid "ARM"
msgstr "ARM"

#: ../../english/releases/arches.data:11
msgid "64-bit ARM (AArch64)"
msgstr "64비트 ARM (AArch64)"

#: ../../english/releases/arches.data:12
msgid "EABI ARM (armel)"
msgstr "EABI ARM (armel)"

#: ../../english/releases/arches.data:13
msgid "Hard Float ABI ARM (armhf)"
msgstr "Hard Float ABI ARM (armhf)"

#: ../../english/releases/arches.data:14
msgid "HP PA-RISC"
msgstr "HP PA-RISC"

#: ../../english/releases/arches.data:15
msgid "Hurd 32-bit PC (i386)"
msgstr "허드 32비트 PC(i386)"

#: ../../english/releases/arches.data:16
msgid "32-bit PC (i386)"
msgstr "32비트 PC (i386)"

#: ../../english/releases/arches.data:17
msgid "Intel Itanium IA-64"
msgstr "인텔 이터니움 IA-64"

#: ../../english/releases/arches.data:18
msgid "kFreeBSD 32-bit PC (i386)"
msgstr "kFreeBSD 32비트 PC (i386)"

#: ../../english/releases/arches.data:19
msgid "kFreeBSD 64-bit PC (amd64)"
msgstr "kFreeBSD 64비트 PC (amd64)"

#: ../../english/releases/arches.data:20
msgid "Motorola 680x0"
msgstr "모토로라 680x0"

#: ../../english/releases/arches.data:21
msgid "MIPS (big endian)"
msgstr "MIPS (big endian)"

#: ../../english/releases/arches.data:22
msgid "64-bit MIPS (little endian)"
msgstr "64비트 MIPS (little endian)"

#: ../../english/releases/arches.data:23
msgid "MIPS (little endian)"
msgstr "MIPS (little endian)"

#: ../../english/releases/arches.data:24
msgid "PowerPC"
msgstr "PowerPC"

#: ../../english/releases/arches.data:25
msgid "POWER Processors"
msgstr "POWER Processors"

#: ../../english/releases/arches.data:26
msgid "RISC-V 64-bit little endian (riscv64)"
msgstr "RISC-V 64비트 little endian (riscv64)"

#: ../../english/releases/arches.data:27
msgid "IBM S/390"
msgstr "IBM S/390"

#: ../../english/releases/arches.data:28
msgid "IBM System z"
msgstr "IBM System z"

#: ../../english/releases/arches.data:29
msgid "SPARC"
msgstr "SPARC"
