#use wml::debian::template title="Informaões de licenciamento" GEN_TIME="yes"
#use wml::debian::translation-check translation="1aeaee4127e2958298108243a68a54e7e17edb99"

<p>Esta página apresenta a opinião de alguns(mas) contribuidores(as)
debian-legal sobre como certas licenças seguem a
<a href="$(HOME)/social_contract#guidelines">Definição Debian de Software
Livre</a> (DFSG). A maioria dessas opiniões foram formadas em
discussões na <a href="https://lists.debian.org/debian-legal/">\
lista de discussão debian-legal</a> em resposta a perguntas de
potenciais mantenedores(as) de pacote ou licenciantes. Consultas de
mantenedores(as) são bem-vindas ao considerar licenças específicas, mas
nós encorajamos a maioria dos(as) mantenedores(as) a usar uma das licenças comuns:
GPL, LGPL, BSD modificada ou Artística.</p>

<p>O software empacotado para o Debian é normalmente classificado em
uma de quatro categorias. Há o software livre (main), software não
livre (non-free), software livre que depende de software não livre
(contrib) e software que não pode ser redistribuído (não incluído).
A <a href="$(DOC)/debian-policy/ch-archive.html">seção 2 da Política
Debian</a> explica exatamente como a DFSG se aplica ao arquivo. Se
estiver em dúvida, é solicitado que os(as) mantenedores(as) enviem um e-mail
sobre as licenças para a debian-legal, incluindo no corpo do e-mail o texto de
qualquer licença nova.
Você pode achar útil <a
href="https://lists.debian.org/search.html">procurar nos arquivos da
lista</a> pelo nome da licença antes de enviar e-mails com
perguntas sobre ela. Se você ainda assim enviar perguntas, por favor mande
os links para algumas das discussões prévias relevantes.</p>

<p>A debian-legal é consultiva. Os verdadeiros responsáveis pelas
decisões são os ftpmasters e os mantenedores(as) dos pacotes. No entanto, se
não for possível convencer a maioria dos(as), geralmente liberais,
contribuidores(as) da debian-legal, provavelmente não está claro se o software
segue a DFSG.</p>

<p>Como quem decide de fato são os ftpmasters e os(as) mantenedores(as) dos
pacotes, é uma ótima ideia consultar <a
href="https://ftp-master.debian.org/REJECT-FAQ.html">o REJECT-FAQ dos
ftpmasters</a> e procurar em site:packages.debian.org por qualquer
licença que você esteja em dúvida, para buscar outros exemplos de como lidar com ela
no Debian. (A busca funciona porque os arquivos de copyright dos
pacotes são publicados em packages.debian.org como texto puro).</p>

<p>Outras listas são mantidas pela <a
href="https://www.gnu.org/licenses/license-list">Free Software
Foundation</a> (FSF) e pela <a
href="https://opensource.org/licenses/">Open Source Initiative</a>
(OSI). Por favor observe que
o projeto Debian decide sobre pacotes específicos ao invés de
resumos de licenças, e as listas contêm explicações gerais.
É possível existir um pacote que contem um software com uma
licença "livre" com algum outro aspecto que o torna não livre.
Às vezes, a debian-legal comenta sobre o resumo de uma licença, sem relação a
nenhum software em particular. Embora essas discussões possam sugerir
possíveis problemas, muitas vezes respostas firmes não podem ser
alcançadas até que algum software específico seja examinado.</p>

<p>Você pode entrar em contato com a debian-legal se tiver questões ou
comentários sobre esses resumos.</p>

<p>Licenças atualmente encontradas no Debian main incluem:</p>

<ul>
<li><a href="https://www.gnu.org/licenses/gpl.html">Licença GNU General Public</a> (common)</li>
<li><a href="https://www.gnu.org/licenses/lgpl.html">Licença GNU Lesser General Public</a> (common)</li>
<li>Licença GNU Library General Public (common)</li>
<li><a href="https://opensource.org/licenses/BSD-3-Clause">Licença Modified BSD</a> (common)</li>
<li><a href="http://www.perl.com/pub/a/language/misc/Artistic.html">Licença Perl Artistic</a> (common)</li>
<li><a href="http://www.apache.org/licenses/">Licença Apache</a></li>
<li><a href="http://www.jclark.com/xml/copying.txt">Licenças Expat/MIT-style</a></li>
<li><a href="https://zlib.net/zlib_license.html">Licenças zlib-style</a></li>
<li><a href="http://www.latex-project.org/lppl/">Licença LaTeX Project Public</a></li>
<li><a href="http://www.python.org/download/releases/2.5.2/license/">Licença Python Software Foundation</a></li>
<li><a href="http://www.ruby-lang.org/en/LICENSE.txt">Licença do Ruby</a></li>
<li><a href="http://www.php.net/license/">Licença PHP</a></li>
<li><a href="http://www.w3.org/Consortium/Legal/2002/copyright-software-20021231">Aviso e Licença W3C Software</a></li>
<li><a href="http://www.openssl.org/source/license.html">Licença OpenSSL</a></li>
<li><a href="https://opensource.org/licenses/Sleepycat">Licença Sleepycat</a></li>
<li><a href="http://www.cups.org/book/examples/LICENSE.txt">Contrato de licenciamento Common UNIX Printing System</a></li>
<li>Licença Pública vhf</li>
<li><a href="http://tunes.org/legalese/bugroff.html">Licença "No problem Bugroff"</a></li>
<li>Licença Unmodified BSD (também conhecida como a original ou
licença BSD de 4 cláusulas. Ela incluía uma exigência e agora foi
descontinuada pelo projeto BSD).</li>
<li>domínio público (não é uma licença, estritamente falando)</li>
<li><a href="http://www.openafs.org/frameset/dl/license10.html">Licença Pública IBM Version 1.0</a></li>
</ul>

<p>Se você usa uma dessas licenças,
por favor tente usar a última versão e editar o mínimo possível,
salvo indicação em contrário.
Licenças marcadas com (common) podem ser encontradas em <tt>/usr/share/common-licenses</tt>
em um sistema Debian.</p>

<p>Licenças atualmente encontradas na seção de arquivo non-free incluem:</p>

<ul>
<li>Licença NVIDIA Software</li>
<li>Licença SCILAB</li>
<li>Contrato de licenciamento Limited Use Software</li>
<li>Licença Non-Commercial</li>
<li>Licença FastCGI / Open Market</li>
<li>Licença LaTeX2HTML</li>
<li>Licença Open Publication</li>
<li>Licença Free Document Dissemination</li>
<li>Licença AT&amp;T Open Source</li>
<li>Licença Apple Public Source</li>
<li>Licença Aladdin Free Public</li>
<li>Generic amiwm License (uma licença XV-style)</li>
<li>Contrato de licenciamento Digital</li>
<li>Licença Moria/Angband</li>
<li>Licença Unarj</li>
<li>Licença id Software</li>
<li>qmail terms</li>
</ul>

<p>Por favor, não faça upload de software com essas licenças para o
arquivo main.</p>

<p>Adicionalmente, certos softwares não são distribuíveis (por
exemplo, se não possuir nenhuma licença), nem mesmo na non-free.</p>


<h2>Trabalho em andamento</h2>

<p>Para ajuda com a interpretação da DFSG, você deve verificar o
<a href="https://people.debian.org/~bap/dfsg-faq">FAQ DFSG</a>
que responde a algumas questões mais comuns sobre a DFSG e como
avaliar software.</p>

