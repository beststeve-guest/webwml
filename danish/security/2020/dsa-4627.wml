#use wml::debian::translation-check translation="0f2e63875207f63029067f8878524f544b09a20f" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Følgende sårbarheder er opdaget i webmotoren webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3862">CVE-2020-3862</a>

    <p>Srikanth Gatta opdagede at et ondsindet websted kunne være i stand til at 
    forårsage et lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3864">CVE-2020-3864</a>

    <p>Ryan Pickren opdagede at en DOM-objektkontekt måske ikke havde et unikt 
    sikkerhedsophav.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3865">CVE-2020-3865</a>

    <p>Ryan Pickren opdagede at en DOM-objektkontekt på øverste niveau 
    fejlagtigt kunne have været betragtet som sikker.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3867">CVE-2020-3867</a>

    <p>En anonym efterforsker opdagede at behandling af ondsindet fabrikeret 
    webindhold kunne føre til universel udførelse af skripter på tværs af 
    servere.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3868">CVE-2020-3868</a>

    <p>Marcin Towalski opdagede at behandling af ondsindet fremstillet 
    webindhold kunne føre til udførelse af vilkårlig kode.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2.26.4-1~deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine webkit2gtk-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende webkit2gtk, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4627.data"
