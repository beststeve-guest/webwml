msgid ""
msgstr ""
"Project-Id-Version: others.po\n"
"PO-Revision-Date: 2012-11-10 23:18+0900\n"
"Last-Translator: Nobuhiro IMAI <nov@yo.rim.or.jp>\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/banners/index.tags:7
msgid "Download"
msgstr "ダウンロード"

#: ../../english/banners/index.tags:11
msgid "Old banner ads"
msgstr "古いバナー"

#: ../../english/devel/debian-installer/ports-status.defs:10
msgid "Working"
msgstr "作業中"

#: ../../english/devel/debian-installer/ports-status.defs:20
msgid "sarge"
msgstr "sarge"

#: ../../english/devel/debian-installer/ports-status.defs:30
msgid "sarge (broken)"
msgstr "sarge (壊れています)"

#: ../../english/devel/debian-installer/ports-status.defs:40
msgid "Booting"
msgstr "起動中"

#: ../../english/devel/debian-installer/ports-status.defs:50
msgid "Building"
msgstr "作成中"

#: ../../english/devel/debian-installer/ports-status.defs:56
msgid "Not yet"
msgstr "まだありません"

#: ../../english/devel/debian-installer/ports-status.defs:59
msgid "No kernel"
msgstr "カーネルなし"

#: ../../english/devel/debian-installer/ports-status.defs:62
msgid "No images"
msgstr "イメージなし"

#: ../../english/devel/debian-installer/ports-status.defs:65
msgid "<void id=\"d-i\" />Unknown"
msgstr "<void id=\"d-i\" />不明"

#: ../../english/devel/debian-installer/ports-status.defs:68
msgid "Unavailable"
msgstr "利用不可"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "新規メンバーのコーナー"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "手順 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "手順 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "手順 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "手順 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "手順 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "手順 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "手順 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "応募者のチェックリスト"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"詳しくは、<a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/"
"french/index.fr.html</a> (フランス語のみ) をご覧ください。"

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "詳細な情報"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"詳しくは、<a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/"
"spanish/</a> (スペイン語のみ) をご覧ください。"

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "電話番号"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "FAX:"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "住所"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "製品"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "Tシャツ"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "帽子"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "ステッカー"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "マグカップ"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "その他衣類"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "ポロシャツ"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "フリスビー"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "マウスパッド"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "バッジ"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "バスケットボールのゴール"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "イヤリング"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "スーツケース"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "傘"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "枕カバー"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "キーチェーン"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "十徳ナイフ"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "USBメモリ"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr ""

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr ""

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr ""

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr ""

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr ""

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr ""

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr ""

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "「Debian」つき"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "「Debian」なし"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "EPS"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Debian を使っています]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Debian GNU/Linux を使っています]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Debian 使用]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (小さなボタン)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "同上"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Debian を使い始めてどれくらいになりますか?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Debian 開発者ですか?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "Debian のどの分野に関わっていますか?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Debian での作業に興味を持ったきっかけは何ですか?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr "Debian にもっと関わることに関心のある女性に何か一言あれば"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr "技術系のグループで他の女性と共同作業をしていますか? あればどこですか?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "他に何か一言あれば"

#: ../../english/y2k/l10n.data:6
msgid "OK"
msgstr "対応"

#: ../../english/y2k/l10n.data:9
msgid "BAD"
msgstr "未対応"

#: ../../english/y2k/l10n.data:12
msgid "OK?"
msgstr "対応?"

#: ../../english/y2k/l10n.data:15
msgid "BAD?"
msgstr "未対応?"

#: ../../english/y2k/l10n.data:18
msgid "??"
msgstr "??"

#: ../../english/y2k/l10n.data:21
msgid "Unknown"
msgstr "不明"

#: ../../english/y2k/l10n.data:24
msgid "ALL"
msgstr "すべて"

#: ../../english/y2k/l10n.data:27
msgid "Package"
msgstr "パッケージ"

#: ../../english/y2k/l10n.data:30
msgid "Status"
msgstr "状況"

#: ../../english/y2k/l10n.data:33
msgid "Version"
msgstr "バージョン"

#: ../../english/y2k/l10n.data:36
msgid "URL"
msgstr "URL"

#~ msgid "Wanted:"
#~ msgstr "必要なハードウェア:"

#~ msgid "Who:"
#~ msgstr "必要としている人/グループ:"

#~ msgid "Architecture:"
#~ msgstr "アーキテクチャ:"

#~ msgid "Specifications:"
#~ msgstr "スペック:"

#~ msgid "Where:"
#~ msgstr "場所:"
