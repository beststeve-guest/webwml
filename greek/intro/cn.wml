#use wml::debian::template title="Οι σελίδες του Debian σε πολλές γλώσσες"
#BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="c25132d79dab5dda5298236044fc2bd05968ef47" maintainer="galaxico"

<protect pass=2>
<:
$lang = languages_footer();
$lang =~ s/<div id="/<div class ="/g;
print $lang;
:>
</protect>

<define-tag toc-title-formatting endtag="required">%body</define-tag>
<define-tag toc-item-formatting endtag="required">[%body]</define-tag>
<define-tag toc-display-begin><p></define-tag>
<define-tag toc-display-end></p></define-tag>

<p>Προφανώς, δεν χρησιμοποιούν όλοι/όλες στον κόσμο την ίδια γλώσσα. Καθώς 
ο παγκόσμιος ιστός μεγαλώνει γίνεται πιο συνηθισμένο να βρίσκει κανείς 
ιστοσελίδες που είναι διαθέσιμες σε πολλαπλές γλώσσες. Εισήχθη, λοιπόν, ένα 
πρότυπο που ονομάζεται
<a href="$(HOME)/devel/website/content_negotiation">διαπραγμάτευση 
περιεχομένου</a>, που επιτρέπει σε κάποιον/α να ορίσει τη γλώσσα (ή γλώσσες) 
της προτίμησής του/της για να βλέπει τα κείμενα στις σελίδες αυτές. Η 
πραγματική εκδοχή που λαμβάνεται είναι "αντικείμενο" διαπραγμάτευσης ανάμεσα 
στον περιηγητή και τον εξυπηρετητή των ιστοσελίδων· ο περιηγητής σας στέλνει 
τις προτιμήσεις του και ο εξυπηρετητής αποφασίζει ποια εκδοχή θα στείλει με 
βάση τις προτιμήσεις σας και την τις εκδοχές του κειμένου που είναι 
διαθέσιμες.</p>

<p>Σημειώστε ότι η επιλογή μια διαφορετικής γλώσσας (από τις μεταφράσεις 
που είναι διαθέσιμες στο τέλος μιας σελίδας) θα δείξουν μόνο την τρέχουσα 
σελίδα στη γλώσσα αυτή. <em>Δεν</em> αλλάζει την προεπιλεγμένη/εξ ορισμού 
γλώσσα. Αν πατήσετε έναν σύνδεσμο σε μια διαφορετική σελίδα, αυτή θα εμφανιστεί 
και πάλι στην αρχική γλώσσα. Για να αλλάξετε την <em>προεπιλεγμένη</em> γλώσσα, 
πρέπει να αλλάξετε τη γλώσσα στις προτιμήσεις των ρυθμίσεων του περιηγητή σας, 
όπως εξηγείται στη συνέχεια..</p>

<p>Μπορείτε να βρείτε επιπρόσθετες πληροφορίες σχετικά με τις επιλογές των 
ρυθμίσεων γλώσσας σ' αυτή τη σελίδα του οργανισμού
<a 
href="http://www.w3.org/International/questions/qa-lang-priorities">W3C</a>.</p>

<ul>
<li><a href="#fix">Τι να κάνετε αν μια ιστοσελίδα του Debian είναι σε λάθος 
γλώσσα</a></li>
<li><a href="#howtoset">Πώς να κάνετε τις ρυθμίσεις της γλώσσας</a></li>
<li>Σε ποιο σημείο να αλλάξετε τις ρυθμίσεις για τους παρακάτω περιηγητές:
  <toc-display /></li>
</ul>

<hr />

<h2><a name="fix">Τι να κάνετε αν μια ιστοσελίδα του Debian είναι σε λάθος 
γλώσσα</a></h2>

<p> Ο πρώτος και πιο συχνός λόγος που ένα κείμενο λαμβάνεται στη λάθος γλώσσα 
από έναν εξυπηρετητή ιστού του Debian είναι ένας λανθασμένα ρυθμισμένος 
περιηγητής. Παρακαλούμε δείτε την ενότητα  <a href="#howtoset">πώς να 
ρυθμίσετε τη γλώσσα προτίμησης</a> για το πώς μπορείτε να το διορθώσετε 
αυτό.</p>

<p>Ο δεύτερος λόγος είναι μια προβληματική ή λάθος ρυθμισμένη cache. Αυτό είναι 
ένα αυξανόμενο πρόβλημα καθώς όλο και περισσότεροι πάροχοι Διαδικτύοτ βλέπουν 
τη χρήση caching σαν έναν τρόπο μείωσης της δικτυακής κίνησης. Διαβάστε τη 
 <a href="#cache">σημείωση σχετικά με caching εξυπηρετητές ιστού</a> ακόμα 
κι αν θεωρείτε ότι δεν χρησιμοποιείτε κάποιον.</p>

<p>Ο τρίτος λόγος είναι να υπάρχει ένα πρόβλημα με τον ιστότοπο 
<a href="https://www.debian.org/">www.debian.org</a>.
Ελάχιστα από τα προβλήματα με τη λήψη λάθος γλώσσας που αναφέρθηκαν τα 
τελευταία χρόνια οφείλονταν στη δική μας πλευρά. Συνιστούμε λοιπόν να 
διερευνήσετε αρχικά τους δύο πρώτους λόγους προβλημάτων εξονυχιστικά πριν 
επικοινωνήσετε μαζί μας. Αν βρείτε ότι ο ιστότοπος  <a 
href="https://www.debian.org/">https://www.debian.org/</a>
δουλεύει κανονικά αλλά όχι κάποιος από τους καθρέφτες του, μπορείτε να μας 
το αναφέρετε κι εμείς θα επικοινωνήσουμε με τους συντηρητές του.</p>

<p>Αφού διορθώσετε οποιαδήποτε από αυτά τα προβλήματα, προτείνουμε να 
καθαρίσετε την τοπική cache (τόσο στον δίσκο όσο και στη μνήμη) στον περιηγητή 
σας πριν προσπαθήσετε να δείτε ξανά τις σελίδες. Προτείνουμε επίσης να 
χρησιμοποιήσετε τον περιηγητή  <a 
href="https://packages.debian.org/stable/web/lynx">lynx</a> όταν κάνετε 
δοκιμές. Είναι ο μόνος περιηγητής που έχουμε βρει ότι είναι συμμορφώνεται 100% 
με τις προδιαγραφές του πρωτοκόλλου HTTP για διαπραγμάτευση περιεχομένου.</p>

<h3><a name="cache">Πιθανά προβλήματα με εξυπηρετητές μεσολάβησης</a></h3>

<p>Οι εξυπηρετητές "μεσολάβησης" (Proxy) είναι ουσιαστικά εξυπηρετητές ιστού 
που δεν έχουν στην πραγματικότητα περιεχόμενο οι ίδιοι. Βρίσκονται ανάμεσα 
στους χρήστες και τους πραγματικούς εξυπηρετητές ιστού. Παίρνουν τα αιτήματά 
σας για ιστοσελίδες και το φέρνουν. Στη συνέχεια, προωθούν τη σελίδα σε σας, 
δημιουργώντας όμως ταυτόχρονα και ένα τοπικό αντόγραφο σε cache για την 
περίπτωση που ζητηθεί αργότερα. Αυτό μπορεί πραγματικά να εξοικονομήσει 
δικτυακή κίνηση όταν πολλοί χρήστες ζητούν την ίδια σελίδα.</p>

<p>Αυτή είναι μια εξαιρετική ιδέα τις περισσότερες φορές αλλά αποτυγχάνει 
όταν η cache είναι προβληματική. Συγκεκριμένα, μερικοί παλιότεροι εξυπηρετητές 
μεσολάβησης δεν καταλαβαίνουν τη διαπραγμάτευση περιεχομένου. Αυτό έχει ως 
αποτέλεσμα να αποθηκεύουν στην cache μια σελίδα σε μια γλώσσα και να σερβίρουν 
αυτή τη σελίδα ακόμα κι αν υπάρχει αίτημα για μια διαφορετική γλώσσα. Η μόνη 
λύση είναι να αναβαθμίσετε ή να αντικαταστήσετε το  λογισμικό για το 
caching.</p>

<p>Ιστορικά, ο κόσμος χρησιμοποιούσε ένα εξυπηρετητή μεσολάβησης μόνο αν είχε 
ρυθμίσει και τον περιηγητή του να χρησιμοποιεί έναν τέτοιο. Αυτό δεν συμβαίνει 
πλέον. Ο πάροχος του Διαδικτύου σας μπορεί να επαναδρομολογεί όλα τα αιτήματα 
HTTP μέσω ενός διαφανούς μεσολαβητή. Αν ο εξυπηρετητής μεσολάβησης δεν 
χειρίζεται σωστά τη διαπραγμάτευση περιεχομένου, τότε οι χρήστες λαμβάνουν 
σελίδες από την cache σε λάθος γλώσσα. Ο μόνος τρόπος να το διορθώσετε αυτό 
είναι να διαμαρτυρηθείτε στον πάροχό σας ζητώντας του να αναβαθμίσει ή να 
αντικαταστήσει το λογισμικό του.</p>

<hr />

<h2><a name="howtoset">Πώς να κάνετε τις ρυθμίσεις της γλώσσας</a></h2>

<p>Θα πρέπει να ορίσετε την γλώσσα προτίμησής σας σε όλες τις γλώσσες που 
μιλάτε, με τη σειρά προτίμησης. Είναι καλή ιδέα να προσθέσετε τα Αγγλικά ('en') 
ως μια επιλογή ασφάλειας (τελευταία στη λίστα) επειδή η πρωτότυπη γλώσσα των 
ιστοσελίδων του Debian είναι τα Αγγλικά και δεν είναι όλα τα κείμενα 
μεταφρασμένα στις γλώσσες της προτίμησής σας.</p>

# translators can modify the below example to mention their language

<p>Για παράδειγμα, αν η μητρική σας γλώσσα είναι τα Ελληνικά, θα θέλετε να 
ρυθμίσετε τη μεταβλητή της γλώσσας ώστε να περιλαμβάνει πρώτα την ελληνική 
γλώσσα (με τον κωδικό γλώσσας '<code>el</code>'), ακολουθούμενη από τα 
Αγγλικά (με τον κωδικό γλώσσας '<code>en</code>').</p>

<p>Δείτε στη συνέχεια <a href="#setting">τις ακριβείς οδηγίες για το πώς 
να το κάνετε αυτό σε συγκεκριμένους περιηγητές</a>.</p>

<p>Όπως θα δείτε εκεί, οι περισσότεροι περιηγητές θα σας προσφέρουν ένα είδος 
διεπαφής χρήστη που θα αποκρύψει κάποιες από τις λεπτομέρειες σχετικά με τον 
ορισμό της γλώσσας προτίμησής σας. Αν δεν συμβαίνει κάτι τέτοιο, παρακαλούμε 
σημειώστε μια σημαντική απλοποίηση στην προηγούμενη παράγραφο: αν προσδιορίσετε 
απλά μια λίστα με γλώσσες όπως 'el, en' αυτό δεν καθορίζει ακόμα μια προτίμηση 
αλλά ισότιμες επιλογές και ίσως ο εξυπηρετητής αγνοήσει τη διάταξή τους. Αν 
θέλετε να καθορίσετε μια πραγματική προτίμηση πρέπει να χρησιμοποιήσετε 
"τιμές ποιότητας" που είναι δεκαδικοί μεταξύ του 0 και του 1, με τη μεγαλύτερη 
τιμή να υποδεικνύει μεγαλύτερη προτίμηση. Έτσι, στην προηγούμενη περίπτωση θα 
πρέπει να χρησιμοποιήσετε πιθανόν κάτι όπως: 'el; q=1.0, en; q=0.5'.</p>

<p>Ένα πράγμα που θα πρέπει να προσέξετε είναι η χρήση υπο-κατηγοριών γλωσσών. 
Η χρήση, για παράδειγμα, 'en-GB, el', δεν κάνει αυτό που ο περισσότερος κόσμος 
θα περίμενε (αν δεν έχουν διαβάσει τις προδιαγραφές του HTTP).</p>

<p><strong>Συνιστούμε ιδιαίτερα να μην προσθέτετε επεκτάσεις χωρών σε μια 
γλώσσα εκτός κι αν έχετε έναν σοβαρό λόγο</strong>. Αν κάνετε κάτι τέτοιο, 
σιγουρευτείτε να συμπεριλάβετα και τη γλώσσα χωρίς την επέκταση.</p>

<p>Εξήγηση: ένας εξυπηρετητής που λαμβάνει ένα αίτημα για ένα κείμενο στην 
προτιμώμενη γλώσσα 'en-GB, el' δεν θα σερβίρει την αγγλική έκδοση ('en') πριν 
από την ελληνική. Θα σερβίρει το αγγλικό κείμενο πριν το ελληνικό εφόσον 
υπάρχει μια έκδοση του αρχείου με την επέκταση 'en-gb' για τη γλώσσα.
</p>

<p>Συνεπώς, θα πρέπει να ρυθμίσετε τον περιηγητή σας να στέλνει 'en-GB, en, el' 
ή απλά 'en, el'. Στην πραγματικότητα το ανάποδο δουλεύει, δηλαδή ένας 
περιηγητής μπορεί να επιστρέψει 'en-us' αν ζητηθεί η 'en'.</p>

<p>Για περισσότερες πληροφορίες σχετικά με τη μεταβλητή της προτιμώμενης 
γλώσσας δείτε τη σελίδα
<a 
href="https://httpd.apache.org/docs/current/content-negotiation.html">
τεκμηρίωση του Apache για τη διαπραγμάτευση περιεχομένου</a>.</p>

<h3><a name="setting">Ορίζοντας τη γλώσσα προτίμησης σε έναν περιηγητή</a></h3>

<p>Για να ορίσετε την προεπιλεγμένη γλώσσα στον περιηγητή σας πρέπει να 
ορίσετε μια μεταβλητή η οποία "περνάει" στον εξυπηρετητή ιστού. Το πώς 
γίνεται αυτό εξαρτάται από τον περιηγητή που χρησιοποιείτε.</p>

<dl>

  <dt><strong><toc-add-entry name="chromium">Chrome και 
Chromium</toc-add-entry></strong></dt>
  <dd>
  <pre>Customize and control Chromium -&gt; Settings -&gt; Show advanced settings -&gt; Languages -&gt; Language and input settings</pre>
  </dd>

  <dt><strong><toc-add-entry name="elinks">ELinks</toc-add-entry></strong></dt>
  <dd>Μπορείτε να ορίσετε την προεπιλεγμένη γλώσσα της διεπαφής στο μενού:
  <pre>
    Setup -&gt; Language
  </pre>
  Αυτό θα αλλάξει επίσης και την γλώσσα που ζητάτε από τους ιστότοπους.
  Μπορείτε να αλλάξετε αυτή τη συμπεριφορά και να κάνετε πιο λεπτομερείς 
ρυθμίσεις της μεταβλητής των αποδεκτών γλωσσών από το HTTP στο:
  <pre>
    Setup -&gt; Options manager -&gt; Protocols -&gt; HTTP
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="epiphany">Epiphany</toc-add-entry></strong></dt>
  <dd>
  <pre>
     Edit -&gt; Preferences -&gt; Language -&gt; Languages
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="mozillafirefox">Firefox</toc-add-entry></strong></dt>
  <dd>
  Έκδοση 3.0 και μεταγενέστερη:<br />
  Linux:
  <pre>
     Edit -&gt; Preferences -&gt; Content -&gt; Languages -&gt; Choose...
  </pre>
  Windows:
  <pre>
     Tools -&gt; Options -&gt; Content -&gt; Languages -&gt; Choose...
  </pre>
  Mac OS:
  <pre>
     Firefox -&gt; Preferences -&gt; Content -&gt; Languages -&gt; Choose...
  </pre>

  <br />
  Έκδοση 1.5 και μεταγενέστερη:<br />
  Linux:
  <pre>
     Edit -&gt; Preferences -&gt; Advanced -&gt; General -&gt; Edit Languages
  </pre>
  Windows:
  <pre>
     Tools -&gt; Options -&gt; Advanced -&gt; General -&gt; Edit Languages
  </pre>

  <br />
  Έκδοση 0.9 και μεταγενέστερη:<br />
  Linux:
  <pre>
     Edit -&gt; Preferences -&gt; General -&gt; Languages
  </pre>
  Windows:
  <pre>
     Tools -&gt; Options -&gt; General -&gt; Languages
  </pre>

  Σε παλιότερες εκδόσεις πρέπει να πάτε στον σύνδεσμο <kbd>about:config</kbd> 
και να αλλάξετε την τιμή της μεταβλητής
  <kbd>intl.accept_languages</kbd>.
  </dd>

  <dt><strong><toc-add-entry name="galeon">Galeon</toc-add-entry></strong></dt>
  <dd>
  <pre>
     Settings -&gt; Preferences -&gt; Rendering -&gt; Languages
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="ibrowse">IBrowse</toc-add-entry></strong></dt>
  <dd>Πηγαίνετε στο Preferences, μετά Settings, και μετά στο Network. Κάτω από 
το "Accept
  language" πιθανόν να δείτε εξ ορισμού ένα "*".  Αν πατήσετε το κουμπί 
  "Locale", θα προσθέσετε την γλώσσα της προτίμησής σας. Αν όχι, μπορείτε να 
την προσθέσετε με το χέρι. Μετά από αυτό, πατήστε "OK".
  </dd>

  <dt><strong><toc-add-entry name="icab">iCab</toc-add-entry></strong></dt>
  <dd>
  <pre>
     Edit -&gt; Preferences -&gt; Browser -&gt; Fonts, Languages
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="iceweasel">Iceweasel</toc-add-entry></strong></dt>
  <dd>
  <pre>
     Edit -&gt; Preferences -&gt; Content -&gt; Languages -&gt; Choose
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="ie">Internet Explorer</toc-add-entry></strong></dt>
  <dd>Windows:
  <pre>
     Tools ή View ή Extras -&gt; Internet Options -&gt; (General) Languages
  </pre>
  </dd>

  <dd>Mac OS:
  <pre>
     Edit -&gt; Preferences -&gt; Web Browser -&gt; Language/Fonts
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="konqueror">Konqueror</toc-add-entry></strong></dt>
  <dd>
  Επεξεργαστείτε το αρχείο
  <kbd>~/.kde/share/config/kio_httprc</kbd> ώστε να προσθέσετε μια γραμμή όπως 
η ακόλουθη:
  <pre>
     Languages=el;q=1.0, en;q=0.5
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="lynx">Lynx</toc-add-entry></strong></dt>
  <dd>Μπορείτε είτε να επεξεργαστείτε τη μεταβλητή
 <kbd>preferred_language</kbd> στο αρχείο
  <kbd>.lynxrc</kbd> είτε να ορίσετε χρησιμοποιώντας την εντολή 'O' ενώ 
έχετε ανοιχτό τον περιηγητή lynx.

  <p>Για παράδειγμα, χρησιμοποιήστε την ακόλουθη γραμμή στο αρχείο 
  <kbd>.lynxrc</kbd>:</p>

  <pre>
  preferred_language=el; q=1.0, en; q=0.5
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="mozilla">Mozilla</toc-add-entry> /
  <toc-add-entry name="netscape">Netscape 4.x</toc-add-entry> και 
μεταγενέστερες εκδόσεις</strong></dt>
  <dd>
  <pre>
     Edit -&gt; Preferences -&gt; Navigator -&gt; Languages
  </pre>
  Σημείωση: με τον Netscape 4.x πρέπει να βεβαιωθείτε ότι διαλέγετε τη γλώσσα 
από τις διαθέσιμες επιλογές. Ένας αριθμός χρηστών έχει αναφέρει προβλήματα 
επειδή όρισαν τη γλώσσα με το χέρι.
  </dd>

  <dt><strong><toc-add-entry name="netscape3">Netscape 3.x</toc-add-entry></strong></dt>
  <dd>Προσθέστε
  <pre>
     *httpAcceptLanguage: [preferred_language string]
  </pre>
  στο αρχείο app-defaults του Netscape ή το αρχείο <kbd>~/.Xresources</kbd>    
  </dd>

  <dt><strong><toc-add-entry name="opera">Opera</toc-add-entry></strong></dt>
  <dd>Οι περισσότερες εκδόσεις:
  <pre>
     File -&gt; Preferences -&gt; Languages
  </pre>
  </dd>
  <dd>Linux/*BSD εκδόσεις 5.x και 6.x:
  <pre>
     File -&gt; Preferences -&gt; Document -&gt; Languages
  </pre>
  </dd>
  <dd>Περιηγητής ιστού Nokia 770:
     Επεξεργαστείτε στο αρχείο /home/user/.opera/opera.ini και προσθέστε την 
ακόλουθη γραμμή στην ενότητα [Adv User Prefs]:
  <pre>
     HTTP Accept Language=el;q=1.0,en;q=0.5
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="pie">Pocket Internet Explorer</toc-add-entry></strong></dt>
  <dd>
  # Windows Mobile 2003/2003SE/5.0
  <pre>
     Δημιουργήστε το registry key <q>AcceptLanguage</q> στο
     HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\International\\
     με τιμή <q>el; q=1.0, en; q=0.5</q> (χωρίς τα εισαγωγικά).
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="safari">Safari</toc-add-entry></strong></dt>
  <dd>Ο περιηγητής Safari χρησιμοποιεί τις προτιμήσεις συστήματος του MacOS X, 
για να καθορίσετε τη γλώσσα προτίμησης:
  <pre>
    System preferences -&gt; International -&gt; Language
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="voyager">Voyager</toc-add-entry></strong></dt>
  <dd>Πηγαίνετε στο Settings, μετά στο Languages.  Μπορείτε είτε να 
προσθέσετε με το χέρι είτε να πατήσετε το "Get from locale".  Πατήστε "OK" όταν 
έχετε τελειώσει.
  </dd>

  <dt><strong><toc-add-entry name="w3">W3</toc-add-entry></strong> 
(περιηγητής ιστού βασισμένος στον emacs)</dt>
  <dd>
  <pre>(setq url-mime-language-string  "preferred_language=el; q=1.0, en; 
q=0.5")</pre>
  ή χρησιμοποιώντας το προσαρμοσμένο πακέτο (υποθέτοντας την έκδοση URL 
p4.0pre.14):
  <pre>Hypermedia -&gt; URL -&gt; Mime -&gt; Mime Language String...</pre>
  </dd>

  <dt><strong><toc-add-entry name="w3m">W3M</toc-add-entry></strong></dt>
  <dd>
  <pre>
     Options (o) -&gt; Other Behavior -&gt; Accept-Language
  </pre>
  </dd>
 
</dl>

<p>Αν έχετε πληροφορίες για τη ρύθμιση ενός περιηγητή που δεν αναφέρεται εδώ, 
παρακαλούμε στείλτε τις στη διεύθυνση 
<a href="mailto:debian-www@lists.debian.org">debian-www@lists.debian.org</a>.</p>
