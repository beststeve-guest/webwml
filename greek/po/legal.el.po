# Emmanuel Galatoulas <galas@tee.gr>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2019-07-14 02:00+0200\n"
"Last-Translator: Emmanuel Galatoulas <galas@tee.gr>\n"
"Language-Team: Greek <debian-www@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/template/debian/legal.wml:15
msgid "License Information"
msgstr "Πληροφορίες Αδειας Χρήσης"

#: ../../english/template/debian/legal.wml:19
msgid "DLS Index"
msgstr "Δείκτης DLS"

#: ../../english/template/debian/legal.wml:23
msgid "DFSG"
msgstr "DFSG"

#: ../../english/template/debian/legal.wml:27
msgid "DFSG FAQ"
msgstr "Συχνές ερωτήσεις DFSG"

#: ../../english/template/debian/legal.wml:31
msgid "Debian-Legal Archive"
msgstr "Debian-Legal Archive"

#. title string without version, on the form "dls-xxx - license name: status"
#: ../../english/template/debian/legal.wml:49
msgid "%s  &ndash; %s: %s"
msgstr "%s  &ndash; %s: %s"

#. title string with version, on the form "dls-xxx - license name, version: status"
#: ../../english/template/debian/legal.wml:52
msgid "%s  &ndash; %s, Version %s: %s"
msgstr "%s  &ndash; %s, Version %s: %s"

#: ../../english/template/debian/legal.wml:59
msgid "Date published"
msgstr "Ημερομηνία Εκδοσης"

#: ../../english/template/debian/legal.wml:61
msgid "License"
msgstr "Αδεια Χρήσης"

#: ../../english/template/debian/legal.wml:64
msgid "Version"
msgstr "Εκδοση"

#: ../../english/template/debian/legal.wml:66
msgid "Summary"
msgstr "Σύνοψη"

#: ../../english/template/debian/legal.wml:70
msgid "Justification"
msgstr "Αιτιολόγηση"

#: ../../english/template/debian/legal.wml:72
msgid "Discussion"
msgstr "Συζήτηση"

#: ../../english/template/debian/legal.wml:74
msgid "Original Summary"
msgstr "Αρχική περίληψη"

#: ../../english/template/debian/legal.wml:76
msgid ""
"The original summary by <summary-author/> can be found in the <a href="
"\"<summary-url/>\">list archives</a>."
msgstr ""
"Η αρχική περίληψη από τον/την <summary-author/> μπορει να βρεθεί<a href="
"\"<summary-url/>\">στην αρχειοθήκη της λίστας</a>."

#: ../../english/template/debian/legal.wml:77
msgid "This summary was prepared by <summary-author/>."
msgstr "Αυτή η περίληψη προετοιμάστηκε από τον/την <summary-author/>."

#: ../../english/template/debian/legal.wml:80
msgid "License text (translated)"
msgstr "Κείμενο της άδειας (μεταφρασμένο)"

#: ../../english/template/debian/legal.wml:83
msgid "License text"
msgstr "Κείμενο άδειας"

#: ../../english/template/debian/legal_tags.wml:6
msgid "free"
msgstr "ελευθερο"

#: ../../english/template/debian/legal_tags.wml:7
msgid "non-free"
msgstr "μη-ελευθερο"

#: ../../english/template/debian/legal_tags.wml:8
msgid "not redistributable"
msgstr "δεν μπορεί να επαναδιανεμηθεί"

#. For the use in headlines, see legal/licenses/byclass.wml
#: ../../english/template/debian/legal_tags.wml:12
msgid "Free"
msgstr "Ελεύθερο"

#: ../../english/template/debian/legal_tags.wml:13
msgid "Non-Free"
msgstr "Μη-Ελεύθερο"

#: ../../english/template/debian/legal_tags.wml:14
msgid "Not Redistributable"
msgstr "Δεν μπορεί να επαναδιανεμηθεί"

#: ../../english/template/debian/legal_tags.wml:27
msgid ""
"See the <a href=\"./\">license information</a> page for an overview of the "
"Debian License Summaries (DLS)."
msgstr ""
"Δείτε τη σελίδα <a href=\"./\">πληροφορίες άδειας</a> για μια επισκόπηση των "
"Debian License Summaries (DLS)."
