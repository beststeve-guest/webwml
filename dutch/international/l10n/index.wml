#use wml::debian::template title="Debian - Centrale vertaalstatistieken"
#use wml::debian::translation-check translation="449f3aabd1b89471275e77a1121210da4f33c4a8"

<p>Deze pagina's tonen het aantal pakketten in Debian die vertaald
kunnen worden en hoeveel er effectief vertaald zijn.</p>

<p>Houd er rekening mee dat dit proces slechts een onderdeel is van
internationalisering (dat afgekort wordt tot <i>i18n</i> omdat er
18 letters staan tussen de "i" en de "n" in het Engelse woord
internationalization). i18n construeert het globaliseringsraamwerk,
terwijl l10n voor elke taal en land eigen "vlees hangt aan dit geraamte".
Daarom hangen de taken die voor l10n moeten worden uitgevoerd af van de
bereikte i18n. Bijvoorbeeld, indien uw i18n-raamwerk u enkel toestaat om
de tekst van een bericht te wijzigen, betekent l10n het louter vertalen van
deze tekst. Indien het i18n-raamwerk u toestaat om de wijze waarop de
datum weergegeven wordt, te wijzigen, kunt u aangeven hoe u dat in uw
deel van de wereld moet doen. Indien het i18n-raamwerk u toestaat om de
tekencodering te veranderen, is l10n het instellen van de tekencodering
die u nodig heeft voor een bepaalde taal. Houd er rekening mee dat voor
sommige (voornamelijk niet-Europese) talen ondersteuning voor tekencodering,
inclusief ondersteuning voor multibyte-tekencodering, tekens van dubbele
breedte, tekensamenvoegingen, tweerichtingstekst, enzovoort, een voorafgaande
vereiste is voor alle andere onderdelen van i18n en l10n, inclusief
vertaling.</p>

<p>l10n en i18n zijn met elkaar verbonden, maar de moeilijkheden die verband
houden met elk van hen, zijn zeer verschillend. Het is niet echt moeilijk
om het programma de weer te geven tekst te laten wijzigen op basis van de
gebruikersinstellingen, maar het is erg tijdrovend om de berichten
daadwerkelijk te vertalen. Anderzijds is het instellen van de tekencodering
triviaal, maar is het aanpassen van de code aan het gebruik van verschillende
tekencoderingen een <a href="$(HOME)/doc/devel-manuals#i18n"><em>heel moeilijk</em></a> probleem.</p>

<p>Hier kunt u enkele statistieken over de l10n van Debian doorbladeren:</p>

<ul>
 <li>Toestand van de <q>l10n</q> met PO-bestanden, m.a.w. hoe goed de pakketten vertaald zijn:
 <ul>
  <li><a href="po/">taallijst</a></li>
  <li><a href="po/rank">rangschikking tussen talen</a></li>
 </ul></li>
 <li>Toestand van de <q>l10n</q> van Debconf-sjabloonbestanden, beheerd via gettext:
 <ul>
  <li><a href="po-debconf/">taallijst</a></li>
  <li><a href="po-debconf/rank">rangschikking tussen talen</a></li>
  <li><a href="po-debconf/pot">originele bestanden</a></li>
 </ul></li>
 <li>Toestand van de <q>l10n</q> van documentatie met PO-bestanden, beheerd met po4a:
 <ul>
  <li><a href="po4a/">taallijst</a></li>
  <li><a href="po4a/rank">rangschikking tussen talen</a></li>
 </ul></li>
 <li><a href="https://ddtp.debian.org/">Toestand van de <q>l10n</q> van Debian pakketbeschrijvingen</a></li>
 <li><a href="$(HOME)/devel/website/stats/">Statistieken over de vertaling van de website van Debian</a></li>
 <li><a href="https://d-i.debian.org/l10n-stats/translation-status.html">Pagina met de vertaalstatus van het Debian Installatiesysteem</a></li>
</ul>
