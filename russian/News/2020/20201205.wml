#use wml::debian::translation-check translation="5c427e44dc4a1503d5262b5edfba60b490ea0ab1" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 10: выпуск 10.7</define-tag>
<define-tag release_date>2020-12-05</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о седьмом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction base-files "Обновление для текущей редакции">
<correction choose-mirror "Обновление списка зеркал">
<correction cups "Исправление неправильного освобождения 'printer-alert'">
<correction dav4tbsync "Новый выпуск основной ветки разработки, совместимый с новыми версиями Thunderbird">
<correction debian-installer "Использование ABI ядра Linux версии 4.19.0-13; добавление grub2 в поле Built-Using">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates">
<correction distro-info-data "Добавление 21.04, Hirsute Hippo">
<correction dpdk "Новый стабильный выпуск основной ветки разработки; исправление удалённого выполнения кода [CVE-2020-14374], проблем TOCTOU [CVE-2020-14375], переполнения буфера [CVE-2020-14376], чтения за пределами выделенного буфера памяти [CVE-2020-14377], отрицательного переполнения целых чисел [CVE-2020-14377]; исправление сборки на armhf с NEON">
<correction eas4tbsync "Новый выпуск основной ветки разработки, совместимый с новыми версиями Thunderbird">
<correction edk2 "Исправление переполнения целых чисел в DxeImageVerificationHandler [CVE-2019-14562]">
<correction efivar "Добавление поддержки для устройств nvme-fabrics и nvme-subsystem; исправление неинициализированной переменной в parse_acpi_root с целью избежать возможной ошибки сегментирования">
<correction enigmail "Добавление ассистента для миграции на встроенную поддержку GPG в Thunderbird">
<correction espeak "Исправление использования espeak с mbrola-fr4, если пакет mbrola-fr1 не установлен">
<correction fastd "Исправление утечки памяти при получении слишком большого числа неправильных пакетов [CVE-2020-27638]">
<correction fish "Проверка того, чтобы опции TTY восстанавливались при выходе">
<correction freecol "Исправление внешней сущности XML [CVE-2018-1000825]">
<correction gajim-omemo "Использование 12-байтового IV для лучшей совместимости в клиентами на iOS">
<correction glances "Прослушивание по адресу localhost по умолчанию">
<correction iptables-persistent "Не загружать модули ядра по методу force-load; улучшение логики сброса правил">
<correction lacme "Использование цепочки сертификатов из основной ветки разработки вместо встроенной цепочки, что облегчает поддержку для нового корневого сертификата Let's Encrypt и промежуточных сертификатов">
<correction libdatetime-timezone-perl "Обновление поставляемых данных tzdata до версии 2020d">
<correction libimobiledevice "Добавление частичной поддержки для iOS 14">
<correction libjpeg-turbo "Исправление отказа в обслуживании [CVE-2018-1152], чтения за пределами выделенного буфера памяти [CVE-2018-14498], удалённого выполнения кода [CVE-2019-2201], чтения за пределами выделенного буфера памяти [CVE-2020-13790]">
<correction libxml2 "Исправление отказа в обслуживании [CVE-2017-18258], разыменования NULL-указателя [CVE-2018-14404], бесконечного цикла [CVE-2018-14567], утечки памяти [CVE-2019-19956 CVE-2019-20388], бесконечного цикла [CVE-2020-7595]">
<correction linux "Новый стабильный выпуск основной ветки разработки">
<correction linux-latest "Обновление для ABI ядра версии 4.19.0-13">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработки">
<correction lmod "Изменение архитектуры на <q>any</q>, потребовалось из-за того, что значения LUA_PATH и LUA_CPATH определяются во время сборки">
<correction mariadb-10.3 "Новый стабильный выпуск основной ветки разработки; исправления безопасности [CVE-2020-14765 CVE-2020-14776 CVE-2020-14789 CVE-2020-14812 CVE-2020-28912]">
<correction mutt "Проверка того, чтобы IMAP-соединение было закрыто после ошибки соединения [CVE-2020-28896]">
<correction neomutt "Проверка того, чтобы IMAP-соединение было закрыто после ошибки соединения [CVE-2020-28896]">
<correction node-object-path "Исправление загрязнения прототипа в set() [CVE-2020-15256]">
<correction node-pathval "Исправление загрязнения прототипа [CVE-2020-7751]">
<correction okular "Исправление выполнения кода через ссылку на действие [CVE-2020-9359]">
<correction openjdk-11 "Новый выпуск основной ветки разработки; исправление аварийной остановки JVM">
<correction partman-auto "Увеличение размера раздела /boot в большинстве рецептов до объёма между 512 и 768 МБ для лучшей обработки изменений ABI ядра и поддержки initramfs большего размера; ограничить размер оперативной памяти, используемый для вычисления размеров раздела подкачки для разрешения проблем на машинах, имеющих больше оперативной памяти, чем дискового пространства">
<correction pcaudiolib "Ограничить задержку отмены команды до 10 миллисекунд">
<correction plinth "Apache: отключение mod_status [CVE-2020-25073]">
<correction puma "Исправление HTTP-инъекции и подделки HTTP [CVE-2020-5247 CVE-2020-5249 CVE-2020-11076 CVE-2020-11077]">
<correction ros-ros-comm "Исправление переполнения целых чисел [CVE-2020-16124]">
<correction ruby2.5 "Исправление подделки HTTP-запроса в WEBrick [CVE-2020-25613]">
<correction sleuthkit "Исправление переполнения буфера в yaffsfs_istat [CVE-2020-10232]">
<correction sqlite3 "Исправление деления на ноль [CVE-2019-16168], разыменования NULL-указателя [CVE-2019-19923], неправильного поведения пустого имени пути во время обновления ZIP-архива [CVE-2019-19925], неправильного поведения встроенных NUL-символов в именах файлов [CVE-2019-19959], аварийной остановки (раскрытие стека WITH) [CVE-2019-20218], переполнения целых чисел [CVE-2020-13434], ошибки сегментирования [CVE-2020-13435], использования указателей после освобождения памяти [CVE-2020-13630], разыменования NULL-указателя [CVE-2020-13632], переполнения динамической памяти [CVE-2020-15358]">
<correction systemd "Basic/cap-list: грамматический разбор/печать численных мандатов; определение новых мандатов из ядра Linux версии 5.8; networkd: отмена создания MAC-адреса для устройства связи сетей">
<correction tbsync "Новый выпуск основной ветки разработки, совместимый с новыми версиями Thunderbird">
<correction tcpdump "Исправление проблемы с недоверенными входными данными в коде печати PPP [CVE-2020-8037]">
<correction tigervnc "Правильное сохранение исключений сертификатов в VNC-просмотрщиках native и java [CVE-2020-26117]">
<correction tor "Новый стабильный выпуск основной ветки разработки; многочисленные исправления безопасности, удобства использования, переносимости и надёжности">
<correction transmission "Исправление утечки памяти">
<correction tzdata "Новый выпуск основной ветки разработки">
<correction ublock-origin "Новая версия основной ветки разработки; разделение дополнений для разных браузеров на разные пакеты">
<correction vips "Исправление использования неинициализированной переменной [CVE-2020-20739]">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2020 4766 rails>
<dsa 2020 4767 mediawiki>
<dsa 2020 4768 firefox-esr>
<dsa 2020 4769 xen>
<dsa 2020 4770 thunderbird>
<dsa 2020 4771 spice>
<dsa 2020 4772 httpcomponents-client>
<dsa 2020 4773 yaws>
<dsa 2020 4774 linux-latest>
<dsa 2020 4774 linux-signed-amd64>
<dsa 2020 4774 linux-signed-arm64>
<dsa 2020 4774 linux-signed-i386>
<dsa 2020 4774 linux>
<dsa 2020 4775 python-flask-cors>
<dsa 2020 4776 mariadb-10.3>
<dsa 2020 4777 freetype>
<dsa 2020 4778 firefox-esr>
<dsa 2020 4779 openjdk-11>
<dsa 2020 4780 thunderbird>
<dsa 2020 4781 blueman>
<dsa 2020 4782 openldap>
<dsa 2020 4783 sddm>
<dsa 2020 4784 wordpress>
<dsa 2020 4785 raptor2>
<dsa 2020 4786 libexif>
<dsa 2020 4787 moin>
<dsa 2020 4788 firefox-esr>
<dsa 2020 4789 codemirror-js>
<dsa 2020 4790 thunderbird>
<dsa 2020 4791 pacemaker>
<dsa 2020 4792 openldap>
<dsa 2020 4793 firefox-esr>
<dsa 2020 4794 mupdf>
<dsa 2020 4795 krb5>
<dsa 2020 4796 thunderbird>
<dsa 2020 4798 spip>
<dsa 2020 4799 x11vnc>
<dsa 2020 4800 libproxy>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за обстоятельств, на которые мы не
можем повлиять:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction freshplayerplugin "Неподдерживается браузерами; прекращена поддержка основной веткой разработки">
<correction nostalgy "Несовместим с новыми версиями Thunderbird">
<correction sieve-extension "Несовместим с новыми версиями Thunderbird">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
