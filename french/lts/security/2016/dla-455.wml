#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6610">CVE-2014-6610</a>

<p>Asterisk Open Source 11.x avant 11.12.1 et 12.x avant 12.5.1, et
Certified Asterisk 11.6 avant 11.6-cert6, permettent, lors de l'utilisation
du module res_fax_spandsp, à des utilisateurs distants authentifiés de
provoquer un déni de service (plantage) à l'aide d'un message « out of
call » qui n'est pas géré correctement dans l'application du plan de
numérotation de ReceiveFax.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-4046">CVE-2014-4046</a>

<p>Asterisk Open Source 11.x avant 11.10.1 et 12.x avant 12.3.1, et
Certified Asterisk 11.6 avant 11.6-cert3 permettent à des utilisateurs
authentifiés Manager d'exécuter des commandes d'interpréteur arbitraires
à l'aide d'une action MixMonitor.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-2286">CVE-2014-2286</a>

<p>main/http.c dans Asterisk Open Source 1.8.x avant 1.8.26.1, 11.8.x
avant 11.8.1 et 12.1.x avant 12.1.1, et Certified Asterisk 1.8.x
avant 1.8.15-cert5 et 11.6 avant 11.6-cert2, permet à des attaquants
distants de provoquer un déni de service (consommation de pile) et
éventuellement d'exécuter du code arbitraire à l'aide d'une requête HTTP
avec un grand nombre d'en-têtes Cookie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-8412">CVE-2014-8412</a>

<p>Les (1) pilotes de canal VoIP, (2) DUNDi, et (3) Asterisk Manager
Interface (AMI) dans Asterisk Open Source 1.8.x avant 1.8.32.1, 11.x
avant 11.14.1, 12.x avant 12.7.1 et 13.x avant 13.0.1, et Certified
Asterisk 1.8.28 avant 1.8.28-cert3 et 11.6 avant 11.6-cert8 permettent
à des attaquants distants de contourner les restrictions ACL à l'aide d'un
paquet dont l'IP source ne partage pas la famille d'adresses IP de la
première entrée ACL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-8418">CVE-2014-8418</a>

<p>La fonction DB dialplan dans Asterisk Open Source 1.8.x avant 1.8.32,
11.x avant 11.1.4.1, 12.x avant 12.7.1 et 13.x avant 13.0.1, et Certified
Asterisk 1.8 avant 1.8.28-cert8 et 11.6 avant 11.6-cert8 permet à des
utilisateurs distants authentifiés d'obtenir des privilèges à l'aide d'un
appel à partir d'un protocole externe, comme démontré par le protocole AMI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-3008">CVE-2015-3008</a>

<p>Asterisk Open Source 1.8 avant 1.8.32.3, 11.x avant 11.17.1, 12.x
avant 12.8.2 et 13.x avant 13.3.2, et Certified Asterisk 1.8.28
avant 1.8.28-cert5, 11.6 avant 11.6-cert11 et 13.1 avant 13.1-cert2, lors
de l'enregistrement d'un périphérique SIP TLS, ne gèrent pas correctement
un octet null dans un nom de domaine dans le champ Common Name (CN) du
sujet d'un certificat X.509. Cela permet à des attaquants de type « homme
du milieu » d'usurper l'identité de serveurs SSL arbitraires à l'aide d'un
certificat contrefait émis par une autorité de certification légitime</p></li>

</ul>

<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans la
version 1:1.8.13.1~dfsg1-3+deb7u4 d'asterisk.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-455.data"
# $Id: $
