#use wml::debian::translation-check translation="a0347b2ff56749e12fbf3c16af552971fffc5eed" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un défaut a été découvert dans la façon dont certaines variables
d’environnement sont évaluées. Un attaquant pourrait utiliser cela pour
outrepasser ou contourner les restrictions d’environnement pour exécuter des
commandes d’interpréteur. Les services et les applications qui permettent à des
attaquants distants non authentifiés de fournir une de ces variables
d’environnement pourraient leur permettre d’exploiter ce problème à distance.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 93u+20120801-3.1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ksh.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ksh, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ksh">https://security-tracker.debian.org/tracker/ksh</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2284.data"
# $Id: $
