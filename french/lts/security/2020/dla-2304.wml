#use wml::debian::translation-check translation="d4f98bcc3f99b9164298226f81ad7a54afc45be2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>add_password dans pam_radius_auth.c dans pam_radius 1.4.0 ne vérifie pas
correctement la longueur du mot de passe fourni et est vulnérable à un
dépassement de pile lors de memcpy(). Un attaquant pourrait envoyer un mot de
passe contrefait à une application (chargeant la bibliothèque pam_radius) et
provoquer son plantage. L’exécution de code arbitraire peut être possible,
selon l’application, la bibliothèque C, le compilateur ou d’autres facteurs.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.3.16-5+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libpam-radius-auth.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libpam-radius-auth, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libpam-radius-auth">https://security-tracker.debian.org/tracker/libpam-radius-auth</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2304.data"
# $Id: $
