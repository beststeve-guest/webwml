#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité a été découverte dans GraphicsMagick, une collection
d'outils de traitement d'images, qui peut avoir pour conséquence un déni de
service.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5685">CVE-2018-5685</a>

<p>Une boucle infinie et un blocage d'application ont été découverts dans
la fonction ReadBMPImage (coders/bmp.c). Des attaquants distants pourraient
utiliser cette vulnérabilité pour provoquer un déni de service à l'aide
d'un fichier image avec une valeur de masque de champ de bits contrefaite.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.3.16-1.1+deb7u17.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphicsmagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1245.data"
# $Id: $
