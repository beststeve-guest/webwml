#use wml::debian::translation-check translation="f5bcaef3918a8a11731afc5ad0381934bf35d171" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été corrigés dans poppler, une bibliothèque de rendu
de PDF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20650">CVE-2018-20650</a>

<p>Une vérification manquante de type de données dict pourrait conduire
à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-21009">CVE-2018-21009</a>

<p>Une dépassement d'entier peut se produire dans Parser::makeStream.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12493">CVE-2019-12493</a>

<p>Une lecture hors limites de tampon basé sur la pile par un fichier PDF
contrefait peut se produire dans PostScriptFunction::transform car quelques
fonctions gèrent incorrectement la transformation de teinte.</p>


<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.26.5-2+deb8u11.</p>
<p>Nous vous recommandons de mettre à jour vos paquets poppler.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1939.data"
# $Id: $
