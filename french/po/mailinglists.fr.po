# Copyright (C) 2008, 2012 Debian French l10n team <debian-l10n-french@lists.debian.org>
#
# Pierre Machard <pmachard@debian.org>, 2008.
# David Prévot <david@tilapin.org>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: debian webwml other 0.1\n"
"PO-Revision-Date: 2012-04-05 10:24-0400\n"
"Last-Translator: David Prévot <david@tilapin.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../../english/MailingLists/mklist.tags:6
msgid "Mailing List Subscription"
msgstr "Abonnement aux listes de diffusion"

#: ../../english/MailingLists/mklist.tags:9
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription web "
"form</a> is also available, for unsubscribing from mailing lists. "
msgstr ""
"Consultez la page sur les <a href=\"./#subunsub\">listes de diffusion</a> "
"pour apprendre à s'abonner par courrier électronique. Un <a href="
"\"unsubscribe\">formulaire de désabonnement</a> est aussi accessible par le "
"web, pour se désabonner des listes de diffusion. "

#: ../../english/MailingLists/mklist.tags:12
msgid ""
"Note that most Debian mailing lists are public forums. Any mails sent to "
"them will be published in public mailing list archives and indexed by search "
"engines. You should only subscribe to Debian mailing lists using an e-mail "
"address that you do not mind being made public."
msgstr ""
"Veuillez noter que les listes de diffusion sont des forums publics. Tout "
"message envoyé à celles-ci sera publié dans des archives publiques de listes "
"de diffusion et indexé par des moteurs de recherche. Vous ne devriez pas "
"vous abonner aux listes de diffusion avec une adresse électronique dont vous "
"jugez la publication problématique."

#: ../../english/MailingLists/mklist.tags:15
msgid ""
"Please select which lists you want to subscribe to (the number of "
"subscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Veuillez choisir les listes auxquelles vous souhaitez vous abonner (le "
"nombre d'inscriptions est limité, veuillez utiliser une <a href=\"./#subunsub"
"\">autre méthode</a> si votre requête n'aboutit pas) :"

#: ../../english/MailingLists/mklist.tags:18
msgid "No description given"
msgstr "Aucune description fournie"

#: ../../english/MailingLists/mklist.tags:21
msgid "Moderated:"
msgstr "Modérée :"

#: ../../english/MailingLists/mklist.tags:24
msgid "Posting messages allowed only to subscribers."
msgstr "Ouverte seulement aux abonnés."

#: ../../english/MailingLists/mklist.tags:27
msgid ""
"Only messages signed by a Debian developer will be accepted by this list."
msgstr ""
"Seuls les messages signés par un développeur Debian sont acceptés par cette "
"liste."

#: ../../english/MailingLists/mklist.tags:30
msgid "Subscription:"
msgstr "Abonnement :"

#: ../../english/MailingLists/mklist.tags:33
msgid "is a read-only, digestified version."
msgstr "est une version résumée en lecture seule"

#: ../../english/MailingLists/mklist.tags:36
msgid "Your E-Mail address:"
msgstr "Votre adresse électronique&nbsp;:"

#: ../../english/MailingLists/mklist.tags:39
msgid "Subscribe"
msgstr "S'abonner"

#: ../../english/MailingLists/mklist.tags:42
msgid "Clear"
msgstr "Effacer"

#: ../../english/MailingLists/mklist.tags:45
msgid ""
"Please respect the <a href=\"./#ads\">Debian mailing list advertising "
"policy</a>."
msgstr ""
"Veuillez respecter la <a href=\"./#ads\">charte relative à la publicité sur "
"les listes de diffusion Debian</a>."

#: ../../english/MailingLists/mklist.tags:48
msgid "Mailing List Unsubscription"
msgstr "Se désabonner des listes de diffusion"

#: ../../english/MailingLists/mklist.tags:51
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription web "
"form</a> is also available, for subscribing to mailing lists. "
msgstr ""
"Consultez la page sur les <a href=\"./#subunsub\">listes de diffusion</a> "
"pour apprendre à se désabonner par courrier électronique. Un <a href="
"\"subscribe\">formulaire d'abonnement</a> est aussi accessible par le web, "
"pour s'abonner aux listes de diffusion. "

#: ../../english/MailingLists/mklist.tags:54
msgid ""
"Please select which lists you want to unsubscribe from (the number of "
"unsubscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Veuillez choisir les listes auxquelles vous souhaitez vous désabonner (le "
"nombre de désinscriptions est limité, veuillez utiliser une <a href=\"./"
"#subunsub\">autre méthode</a> si votre requête n'aboutit pas) :"

#: ../../english/MailingLists/mklist.tags:57
msgid "Unsubscribe"
msgstr "Se désabonner"

#: ../../english/MailingLists/mklist.tags:60
msgid "open"
msgstr "ouverte"

#: ../../english/MailingLists/mklist.tags:63
msgid "closed"
msgstr "fermée"

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr ""
#~ "Veuillez choisir les listes desquelles vous souhaitez vous "
#~ "désabonner&nbsp;:"

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr ""
#~ "Veuillez choisir les listes auxquelles vous souhaitez vous abonner&nbsp;:"
