#use wml::debian::translation-check translation="ae06255bde57d831150a61d8cba709fe69cdbd83" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation de privilèges, un déni de service, ou
une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-2732">CVE-2020-2732</a>

<p>Paulo Bonzini a découvert que l'implémentation de KVM pour les
processeurs Intel ne gérait pas correctement l'émulation d'instruction pour
des clients L2 quand la virtualisation imbriquée est activée. Cela pourrait
permettre à un client L2 de provoquer une élévation de privilèges, un déni
de service, ou des fuites d'informations dans le client L1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8428">CVE-2020-8428</a>

<p>Al Viro a découvert une vulnérabilité d'utilisation de mémoire après
libération dans la couche VFS. Cela permettait à des utilisateurs locaux de
provoquer un déni de service (plantage) ou d'obtenir des informations
sensibles à partir de la mémoire du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10942">CVE-2020-10942</a>

<p>Le pilote vhost_net ne validait pas correctement le type des sockets
configurés comme « backend ». Un utilisateur local autorisé à accéder
à /dev/vhost-net pourrait utiliser cela pour provoquer une corruption de pile
au moyen d'appels système contrefaits, avec pour conséquence un déni de
service (plantage) ou éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11565">CVE-2020-11565</a>

<p>Entropy Moe a signalé que le système de fichiers de la mémoire partagée
(tmpfs) ne gérait pas correctement une option de montage <q>mpol</q> en
indiquant une liste de nœuds vide, menant à une écriture de pile hors
limites. Si les espaces de noms utilisateur sont activés, un utilisateur
local pourrait utiliser cela pour provoquer un déni de service (plantage)
ou éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11884">CVE-2020-11884</a>

<p>Al Viro a signalé une situation de compétition dans le code de gestion
de mémoire pour IBM Z (architecture s390x) qui peut avoir pour conséquence
l'exécution de code par le noyau à partir de l'espace d'adresses
utilisateur. Un utilisateur local pourrait utiliser cela pour une élévation
de privilèges.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 4.19.98-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4667.data"
# $Id: $
