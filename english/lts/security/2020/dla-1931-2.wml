<define-tag description>LTS regression update</define-tag>
<define-tag moreinfo>

<p>It was discovered that the fix to address an ECDSA timing attack in
the <tt>libgcrypt20</tt> cryptographic library was incomplete.</p>

<p>Thanks to Albert Chin-A-Young &lt;china@thewrittenword.com&gt; for
the report.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13627">CVE-2019-13627</a>

    <p>It was discovered that there was a ECDSA timing attack in the libgcrypt20 cryptographic library.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.6.3-2+deb8u8.</p>

<p>We recommend that you upgrade your libgcrypt20 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-1931-2.data"
# $Id: $
