<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update of pacemaker released as DLA-2447-1 caused a regression when the
communication between the Corosync cluster engine and pacemaker takes place. A
permission problem prevents IPC requests between cluster nodes. The patch for
<a href="https://security-tracker.debian.org/tracker/CVE-2020-25654">CVE-2020-25654</a> has been reverted until a better solution can be found.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.1.16-1+deb9u2.</p>

<p>We recommend that you upgrade your pacemaker packages.</p>

<p>For the detailed security status of pacemaker please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/pacemaker">https://security-tracker.debian.org/tracker/pacemaker</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>
</pre><p><strong>Attachment:
<a href="https://lists.debian.org/debian-lts-announce/2020/11/pgpJhKIPYGUAd.pgp"><tt>signature.asc</tt></a></strong><br>
<em>Description:</em> This is a digitally signed message part</p></p>

<p><!--X-Body-of-Message-End-->
<!--X-MsgBody-End-->
<!--X-Follow-Ups-->
<hr>
<strong>Reply to:</strong>
<ul>
  <li><a href="mailto:debian-lts-announce@lists.debian.org?in-reply-to=&lt;22dc8c85d4840ae3ca44e8e0bf7f72eb69d1157b.camel@debian.org&gt;&amp;subject=Re:%20[SECURITY] [DLA 2447-2] pacemaker regression update">debian-lts-announce@lists.debian.org</a></li>
  <li><a href="mailto:apo@debian.org?in-reply-to=&lt;22dc8c85d4840ae3ca44e8e0bf7f72eb69d1157b.camel@debian.org&gt;&amp;subject=Re:%20[SECURITY] [DLA 2447-2] pacemaker regression update&amp;cc�bian-lts-announce@lists.debian.org">Markus Koschany (on-list)</a></li>
  <li><a href="mailto:apo@debian.org?in-reply-to=&lt;22dc8c85d4840ae3ca44e8e0bf7f72eb69d1157b.camel@debian.org&gt;&amp;subject=Re:%20[SECURITY] [DLA 2447-2] pacemaker regression update">Markus Koschany (off-list)</a></li>
</ul>
<hr>
<!--X-Follow-Ups-End-->
<!--X-References-->
<!--X-References-End-->
<!--X-BotPNI-->
<ul>
<li>Prev by Date:
<strong><a href="https://lists.debian.org/debian-lts-announce/2020/11/msg00028.html">[SECURITY] [DLA 2452-2] libdatetime-timezone-perl regression update</a></strong>
</li>
<li>Next by Date:
<strong><a href="https://lists.debian.org/debian-lts-announce/2020/11/msg00030.html">[SECURITY] [DLA 2454-1] rclone security update</a></strong>
</li>
<li>Previous by thread:
<strong><a href="https://lists.debian.org/debian-lts-announce/2020/11/msg00028.html">[SECURITY] [DLA 2452-2] libdatetime-timezone-perl regression update</a></strong>
</li>
<li>Next by thread:
<strong><a href="https://lists.debian.org/debian-lts-announce/2020/11/msg00030.html">[SECURITY] [DLA 2454-1] rclone security update</a></strong>
</li>
<li>Index(es):
<ul>
<li><a href="https://lists.debian.org/debian-lts-announce/2020/11/maillist.html#00029"><strong>Date</strong></a></li>
<li><a href="https://lists.debian.org/debian-lts-announce/2020/11/threads.html#00029"><strong>Thread</strong></a></li>
</ul>
</li>
</ul></p>

<p><!--X-BotPNI-End-->
<!--X-User-Footer-->
<!--X-User-Footer-End--></p>


<p></body></html></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2447-2.data"
# $Id: $
