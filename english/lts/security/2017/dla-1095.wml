<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tyler Bohan of Talos discovered that FreeRDP, a free implementation of
the Remote Desktop Protocol (RDP), contained several vulnerabilities
that allowed a malicious remote server or a man-in-the-middle to
either cause a DoS by forcibly terminating the client, or execute
arbitrary code on the client side.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.0.1-1.1+deb7u4.</p>

<p>We recommend that you upgrade your freerdp packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1095.data"
# $Id: $
