<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7953">CVE-2016-7953</a>

      <p>If an empty string is received from an x-server, do not underrun
      the buffer by accessing "rep.nameLen - 1" unconditionally, which
      could end up being -1.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:1.0.7-1+deb7u3.</p>

<p>We recommend that you upgrade your libxvmc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-671.data"
# $Id: $
