<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Guillaume Teissier reported that the XMLRPC client in libxmlrpc3-java,
an XML-RPC implementation in Java, does perform deserialization of the
server-side exception serialized in the faultCause attribute of XMLRPC
error response messages. A malicious XMLRPC server can take advantage of
this flaw to execute arbitrary code with the privileges of an
application using the Apache XMLRPC client library.</p>

<p>Note that a client that expects to get server-side exceptions need to
set explicitly the enabledForExceptions property.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 3.1.3-8+deb9u1.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 3.1.3-9+deb10u1.</p>

<p>We recommend that you upgrade your libxmlrpc3-java packages.</p>

<p>For the detailed security status of libxmlrpc3-java please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxmlrpc3-java">https://security-tracker.debian.org/tracker/libxmlrpc3-java</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4619.data"
# $Id: $
