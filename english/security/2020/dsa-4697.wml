<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>A flaw was reported in the TLS session ticket key construction in
GnuTLS, a library implementing the TLS and SSL protocols. The flaw
caused the TLS server to not securely construct a session ticket
encryption key considering the application supplied secret, allowing a
man-in-the-middle attacker to bypass authentication in TLS 1.3 and
recover previous conversations in TLS 1.2.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 3.6.7-4+deb10u4.</p>

<p>We recommend that you upgrade your gnutls28 packages.</p>

<p>For the detailed security status of gnutls28 please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gnutls28">https://security-tracker.debian.org/tracker/gnutls28</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4697.data"
# $Id: $
